package me.dablakbandit.youtubedl;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.AbstractButton;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Enumeration;

import javax.swing.JTextPane;
import javax.swing.JProgressBar;

import java.awt.Color;

import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

@SuppressWarnings("serial")
public class Interface extends JFrame {

	private JPanel contentPane;
	private JTextField txtUrl;
	private JTextPane txtOutput;
	private JTextField txtSize;
	private JProgressBar progressBar;
	private JTextField txtDestination;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Interface frame = new Interface();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public Interface() {
		setTitle("Youtube-DL-Music");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		btnDownload = new JButton("Download");
		btnDownload.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				download();
			}
		});
		btnDownload.setBounds(315, 128, 109, 23);
		contentPane.add(btnDownload);

		txtUrl = new JTextField();
		txtUrl.setBounds(93, 11, 331, 20);
		contentPane.add(txtUrl);
		txtUrl.setColumns(10);

		JLabel lblUrl = new JLabel("URL:");
		lblUrl.setBounds(10, 14, 46, 14);
		contentPane.add(lblUrl);

		txtOutput = new JTextPane();
		txtOutput.setEditable(false);
		txtOutput.setBounds(93, 194, 331, 48);
		contentPane.add(txtOutput);

		JLabel lblOutput = new JLabel("Output:");
		lblOutput.setBounds(10, 212, 46, 14);
		contentPane.add(lblOutput);

		progressBar = new JProgressBar();
		progressBar.setBounds(93, 132, 212, 14);
		contentPane.add(progressBar);

		txtSize = new JTextField();
		txtSize.setBackground(Color.WHITE);
		txtSize.setEditable(false);
		txtSize.setColumns(10);
		txtSize.setBounds(134, 97, 78, 20);
		contentPane.add(txtSize);

		JLabel lblSize = new JLabel("Size:");
		lblSize.setBounds(93, 100, 46, 14);
		contentPane.add(lblSize);

		txtDestination = new JTextField();
		txtDestination.setBackground(Color.WHITE);
		txtDestination.setEditable(false);
		txtDestination.setColumns(10);
		txtDestination.setBounds(93, 162, 331, 20);
		contentPane.add(txtDestination);

		JLabel lblDestination = new JLabel("Destination:");
		lblDestination.setBounds(10, 165, 73, 14);
		contentPane.add(lblDestination);

		JLabel lblProgress = new JLabel("Progress:");
		lblProgress.setBounds(10, 132, 58, 14);
		contentPane.add(lblProgress);

		JLabel lblCodec = new JLabel("Codec:");
		lblCodec.setBounds(10, 42, 58, 14);
		contentPane.add(lblCodec);

		JRadioButton btnmp3 = new JRadioButton("mp3");
		buttonGroup.add(btnmp3);
		btnmp3.setBounds(252, 38, 51, 23);
		contentPane.add(btnmp3);

		JRadioButton btnac3 = new JRadioButton("ac3");
		buttonGroup.add(btnac3);
		btnac3.setBounds(199, 38, 51, 23);
		contentPane.add(btnac3);

		JRadioButton btnaac = new JRadioButton("aac");
		buttonGroup.add(btnaac);
		btnaac.setBounds(146, 38, 51, 23);
		contentPane.add(btnaac);

		JRadioButton btnm4a = new JRadioButton("m4a");
		btnm4a.setSelected(true);
		buttonGroup.add(btnm4a);
		btnm4a.setBounds(93, 38, 51, 23);
		contentPane.add(btnm4a);

		JRadioButton btnogg = new JRadioButton("ogg");
		buttonGroup.add(btnogg);
		btnogg.setBounds(305, 38, 51, 23);
		contentPane.add(btnogg);

		txtSpeed = new JTextField();
		txtSpeed.setEditable(false);
		txtSpeed.setColumns(10);
		txtSpeed.setBackground(Color.WHITE);
		txtSpeed.setBounds(278, 97, 78, 20);
		contentPane.add(txtSpeed);

		JLabel lblSpeed = new JLabel("Speed:");
		lblSpeed.setBounds(228, 100, 51, 14);
		contentPane.add(lblSpeed);

		JLabel lblState = new JLabel("State:");
		lblState.setBounds(93, 69, 46, 14);
		contentPane.add(lblState);

		txtState = new JTextField();
		txtState.setEditable(false);
		txtState.setColumns(10);
		txtState.setBackground(Color.WHITE);
		txtState.setBounds(134, 66, 78, 20);
		contentPane.add(txtState);

		JLabel lblEta = new JLabel("ETA:");
		lblEta.setBounds(228, 68, 51, 14);
		contentPane.add(lblEta);

		txtEta = new JTextField();
		txtEta.setEditable(false);
		txtEta.setColumns(10);
		txtEta.setBackground(Color.WHITE);
		txtEta.setBounds(278, 66, 78, 20);
		contentPane.add(txtEta);
	}

	public DownloadThread dt;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JButton btnDownload;
	private JTextField txtSpeed;
	private JTextField txtState;
	private JTextField txtEta;
	
	private File currentDir = new File("");
	private File ffmpeg = new File(currentDir.getAbsoluteFile(), File.separator + "FFMPEG" + File.separator + "bin" + File.separator + "ffmpeg.exe");

	public void download(){
		if(dt!=null&&dt.getRunning())return;
		btnDownload.setEnabled(false);
		dt = new DownloadThread();
		Thread t = new Thread(dt);
		t.start();
	}

	public class DownloadThread implements Runnable{

		public boolean running = false;

		public String ac;

		@Override
		public void run() {
			running = true;
			txtSize.setText("");
			txtDestination.setText("");
			String best = getBestFormat();
			if(best.equals("")){
				txtState.setText("Failed");
			}else{
				try{
					txtState.setText("Downloading");
					Runtime rt = Runtime.getRuntime();
					String command = "youtube-dl.exe " + txtUrl.getText() + " -f " + best + " --no-playlist --no-part --no-continue -o %(title)s.%(ext)s --ffmpeg-location \"" + ffmpeg.getAbsolutePath() + "\"";
					Process proc = rt.exec(command);

					BufferedReader stdInput = new BufferedReader(new 
							InputStreamReader(proc.getInputStream()));

					BufferedReader stdError = new BufferedReader(new 
							InputStreamReader(proc.getErrorStream()));

					String s = null;
					while ((s = stdInput.readLine()) != null) {
						process(s);
					}
					while ((s = stdError.readLine()) != null) {
						process(s);
					}
					txtEta.setText("");
					txtSpeed.setText("");
					convert();
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			btnDownload.setEnabled(true);
			running = false;
		}

		public String getBestFormat(){
			try{
				txtState.setText("Checking");
				Runtime rt = Runtime.getRuntime();
				String command = "youtube-dl.exe " + txtUrl.getText() + " --no-playlist -F";
				Process proc = rt.exec(command);

				BufferedReader stdInput = new BufferedReader(new 
						InputStreamReader(proc.getInputStream()));

				String s = null;
				String b = "";
				while ((s = stdInput.readLine()) != null) {
					process2(s);
					try{
						while(s.contains("  "))s = s.replaceAll("  ", " ");
						String a[] = s.split(" ");
						Integer.parseInt(a[0]);
						if(a[1].equals("m4a")&&a[2].equals("audio")){
							b = a[0];
						}
					}catch(Exception e){}
				}
				return b;
			}catch(Exception e){
				e.printStackTrace();
			}
			return "";
		}

		public void convert(){
			try{
				Runtime rt = Runtime.getRuntime();
				File f2 = new File(currentDir.getAbsoluteFile(), txtDestination.getText());
				String type = getSelectedButtonText(buttonGroup);
				if(f2.exists()&&!type.equals("m4a")){
					txtState.setText("Converting");
					String file = f2.getAbsolutePath();
					String file1 = file.substring(0, file.length()-3) + type;
					String command = ffmpeg.getAbsolutePath() + " -i \"" + file + "\" -y -acodec " + getFormat(type) + " \"" + file1 + "\"";
					Process proc = rt.exec(command);

					BufferedReader stdError = new BufferedReader(new 
							InputStreamReader(proc.getErrorStream()));

					String s = null;
					while ((s = stdError.readLine()) != null) {
						process2(s);
					}
					f2.delete();
					File f3 = new File(file1);
					txtSize.setText(formatSize(f3.length()));
					txtDestination.setText(f3.getName());
				}
				txtState.setText("Finished");
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		public String formatSize(double size){
			return String.format("%.2f", new Object[]{size/1024/1024}) + "MiB";
		}

		public String getSelectedButtonText(ButtonGroup buttonGroup) {
			for(Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
				AbstractButton button = buttons.nextElement();
				if (button.isSelected()) {
					return button.getText();
				}
			}
			return null;
		}

		public String getFormat(String s){
			switch(s){
			case "m4a":break;
			case "aac":return "aac";
			case "ac3":return "ac3";
			case "mp3":return "mp3";
			case "ogg":return "libvorbis";
			}
			return "m4a";
		}

		public void process(String s){
			try{
				if(s.startsWith("[download]")){
					String s1 = s.replace("[download]", "");
					while(s1.startsWith(" ")){
						s1 = s1.substring(1);
					}
					if(s1.startsWith("Destination:")){
						txtDestination.setText(s1.replace("Destination: ", ""));
					}else if(s1.startsWith("100%")){
						progressBar.setValue(100);
					}else if(s1.startsWith("Resuming")){

					}else{
						String[] a = s1.split(" ");
						int i = (int)Math.round(Double.parseDouble(a[0].replaceAll("%", "")));
						if(txtSize.getText().equals("")){
							txtSize.setText(a[2]);
						}
						progressBar.setValue(i);
						txtSpeed.setText(a[4]);
						txtEta.setText(a[6]);
					}
				}else if(s.startsWith("[ffmpeg]")){
					String s1 = s.replace("[ffmpeg]", "");
					while(s1.startsWith(" ")){
						s1 = s1.substring(1);
					}
					if(s1.startsWith("Destination:")){
						txtDestination.setText(s1.replace("Destination:", ""));
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			txtOutput.setText(s);
		}

		public void process2(String s){
			txtOutput.setText(s);
		}

		public boolean getRunning(){
			return running;
		}

	}
}
